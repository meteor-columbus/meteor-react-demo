import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data'

import { Tasks } from './api/tasks'

import Task from './ui/Task.jsx'
import AccountsUIWrapper from './ui/AccountsUIWrapper.jsx'

//main client component. the primary container component used to house the rest of the app
class App extends Component {
    //getInitialState for componenets within es6 is replaced by using the constructor property
    constructor(props) {
        super(props);

        this.state = {
            hideCompleted: false,
        };
    }

    handleSubmit(event) {
        event.preventDefault()

        //grabs the submit field via ref. refs are properties the component aka comp.ref['your ref']
        const input = ReactDOM.findDOMNode(this.refs.textInput)
        const text = input.value.trim()

        //Tasks is basically a mongo db obj. so .insert,.update, .find, ...ect
        //mongo doesn't require schemes. blessing and a curse
        //Tasks.insert({
        //    text, //automatically declares name of prop if not declared
        //    createdAt: new Date(),
        //    owner: Meteor.userId(),           // _id of logged in user
        //    username: Meteor.user().username,  // username of logged in user
        //})

        //changing to go through meteors methods that are declared in tasks for security
        Meteor.call('tasks.insert', text);

        input.value =''

    }
    //setState is asyn, passing a callback to it will allow synchronous functionality
    toggleHideCompleted() {
        this.setState({
            hideCompleted: !this.state.hideCompleted,
        });
    }


    renderTasks() {
        let filteredTasks = this.props.tasks;
        if (this.state.hideCompleted) {
            filteredTasks = filteredTasks.filter(task => !task.checked);
        }
        return filteredTasks.map((task) => {
            const currentUserId = this.props.currentUser && this.props.currentUser._id;
            const showPrivateButton = task.owner === currentUserId;

            return (
                <Task
                    key={task._id}
                    task={task}
                    showPrivateButton={showPrivateButton}
                />
            );
        });
    }

    render() {
        return (
            <div className="container">
                <header>
                    <h1>Things That Need Doing ({this.props.incompleteCount})</h1>

                    <label className="hide-completed">
                        <input
                            type="checkbox"
                            readOnly
                            checked={this.state.hideCompleted}
                            onClick={this.toggleHideCompleted.bind(this)}
                        />
                        Hide Completed Tasks
                    </label>

                    <AccountsUIWrapper />

                    { this.props.currentUser ?
                        <form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
                            <input
                                type="text"
                                ref="textInput"
                                placeholder="Type to add new tasks"
                            />
                        </form> : ''
                    }
                </header>

                <ul>
                    {this.renderTasks()}
                </ul>
            </div>
        )
    }
}

App.propTypes = {
    tasks: PropTypes.array.isRequired,
    incompleteCount: PropTypes.number.isRequired,
    currentUser: PropTypes.object,
};

//wraps the class/component in a container that comes from react-meteor-data - *meteor add react-meteor-data*
export default createContainer(() => {
    //allows access to the tasks publish stream
    Meteor.subscribe('tasks');

    return {
        //find takes a query filter object {}, and can take a "projection" object
        tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),
        incompleteCount: Tasks.find({ checked: { $ne: true } }).count(),
        currentUser: Meteor.user(),
    };
}, App);